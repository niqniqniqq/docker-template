package main

import (
	"net/http"
    "fmt"
    "os"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "Hello Wolrd")
	})

	v1 := router.Group("api/v1")
	{
		v1.GET("/trace", ddTrace)
	}

	router.Run(":3245")
}

func ddTrace(c *gin.Context) {
    ip := os.Getenv("IP")
    fmt.Println(ip)
    apiKey := os.Getenv("API_KEY")
    fmt.Println(apiKey)
    appKey := os.Getenv("APP_KEY")
    fmt.Println(appKey)
	c.JSON(http.StatusOK, gin.H{"status": "you are logged in"})
}